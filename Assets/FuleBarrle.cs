﻿using UnityEngine;
using System.Collections;

public class FuleBarrle : Explosion
{
    [SerializeField]
    float ExplosionDamage, Range, ExplosionTime;
    Attributes attributes;

    void Update()
    {
        if (attributes == null) attributes = GetComponent<Attributes>();

        if (attributes.IsDead())
        {
            StartCoroutine(Boom());
        }
    }

    IEnumerator Boom()
    {
        yield return new WaitForSeconds(ExplosionTime);
        StartCoroutine(Explode(transform.position, Range, ExplosionDamage, GlobleLayers.DetectAllMask));
        attributes.Kill();
        gameObject.SetActive(false);
    }

}
