﻿using UnityEngine;
using System.Collections;

public class ScoreTracker : MonoBehaviour {

    public static int ComboScore;
    public static int HitCombo;

    public static int TotalScore;

    public static void AddHitScore(int AddScore)
    {
        HitCombo++;
        ComboScore += AddScore * HitCombo;
    }

    public static void BrakeCombo()
    {
        TotalScore += ComboScore;
        HitCombo = 0;
        ComboScore = 0;
    }
}
