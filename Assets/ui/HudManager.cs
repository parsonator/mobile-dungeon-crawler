﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HudManager : MonoBehaviour {

    [SerializeField]
    Image GunMask, HealthBar;
    [SerializeField]
    Text AmmoCounter,Score,HitCombo,ComboScore;



	// Use this for initialization

	// Update is called once per frame
	void Update () {
        AmmoCounter.text = (PlayerCombat.MainPlayerCombat.MaxAmmo + "/" + PlayerCombat.MainPlayerCombat.CurruntAmmo);
        GunMask.fillAmount = (PlayerCombat.MainPlayerCombat.CurruntAmmo/ PlayerCombat.MainPlayerCombat.MaxAmmo);
        HealthBar.fillAmount = PlayerMovement.MainPlayer.PlayerAttributes.GetHeathPercent();

        Score.text = "" + ScoreTracker.TotalScore;

        if (ScoreTracker.HitCombo != 0)
        {
            HitCombo.text = "" + ScoreTracker.HitCombo + "*";
            ComboScore.text = "" + ScoreTracker.ComboScore;
        }
        else
        {
            HitCombo.text = "";
            ComboScore.text = "";
        }
        
	}
}
