﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class CanvasScreenManager : MonoBehaviour
{

    public static CanvasScreenManager Master;
    public CanvasScreen StartScreen;
    [Space(5)]
    public List<CanvasScreen> Screens;

    void Start()
    {
        if (Master == null)
            Master = this;
        SetScreens(StartScreen);
    }

    public void SetScreens(CanvasScreen ScreenName)
    {
        foreach (CanvasScreen screens in Screens)
        {
            if (screens.ScreenName == ScreenName.ScreenName)
            {
                screens.ScreenShowing = true;
            }
            else
            {
                screens.ScreenShowing = false;
            }
        }
    }

    public CanvasScreen FindScreen(string ScreenName)
    {

        foreach(CanvasScreen screens in Screens)
        {
            if(screens.ScreenName == ScreenName)
            {
                return screens;
            }
        }

        return null;

    }


}
