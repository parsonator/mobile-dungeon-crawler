﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CanvasScreen : MonoBehaviour {

    public string ScreenName
    {
        get
        {
            return gameObject.name;
        }
    }
    public bool ScreenShowing;

    Transform screen
    {
        get
        {
            return transform.FindChild("Screen");
        }
    }        

    Animator anim;

	void Start ()
    {
        anim = GetComponent<Animator>();
	}
	
	void Update ()
    {
        if (ScreenShowing)
        {
            ScreenIn();
        }
        else
        {
            ScreenOut();
        }
	}

    void ScreenIn()
    {
        ScreenTransition(true);
    }

    void ScreenOut()
    {
        ScreenTransition(false);
    }

    void ScreenTransition(bool Active)
    {
        anim.SetBool("ScreenShowing", Active);


    }

}
