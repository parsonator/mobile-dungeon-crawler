﻿using UnityEngine;
using System.Collections;

public class Bomb : Explosion
{

    public float Range, Damage;
    public LayerMask Mask;


    void OnCollisionEnter(Collision collision)
    {
        StartCoroutine(Explode(transform.position,Range,Damage, GlobleLayers.DetectAllMask));
        StartCoroutine(PlayerCamera.Shake(1,0.5f));
        Destroy(gameObject,0.05f);
    }
}
