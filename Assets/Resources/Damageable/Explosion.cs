﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour
{
    static GameObject ExplosionOjb;
    ParticleSystem ExplosionFX;

    bool FuzeLit;

    void Awake()
    {
        if (ExplosionOjb == null)
            ExplosionOjb = Resources.Load<GameObject>("Effects/explosion");
        ExplosionSetup();
    }

    void ExplosionSetup()
    {
        GameObject Particles;
        Particles = Instantiate(ExplosionOjb, transform.position, Quaternion.identity) as GameObject;
        Particles.transform.parent = transform;
        ExplosionFX = Particles.GetComponent<ParticleSystem>();
    }


    public IEnumerator Explode(Vector3 Origin, float Size, float ExplodeDamage,LayerMask Mask, float Fuze = 0)
    {
        if (FuzeLit) yield break;


        if (Fuze > 0)
        {
            FuzeLit = true;
            yield return new WaitForSeconds(Fuze);

        }

        ExplosionFX.transform.localScale = (Vector3.one) * Size;
        ExplosionFX.transform.position = Origin;
        ExplosionFX.transform.parent = null;
        ExplosionFX.Play();

        Collider[] hits = Physics.OverlapSphere(Origin, Size/2, Mask);
        foreach (Collider col in hits)
        {
            col.SendMessageUpwards("Damage", ExplodeDamage, SendMessageOptions.DontRequireReceiver);
        }
        Destroy(ExplosionFX.gameObject, ExplosionFX.duration);

        yield return new WaitForSeconds(0.1f);

    }



}
