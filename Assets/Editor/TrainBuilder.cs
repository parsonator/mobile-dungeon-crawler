﻿using UnityEngine;
using UnityEditor;

using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(TrainBuilder))]
public class TrainBuilde : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        EditorGUILayout.LabelField("Train Carrages will be in Hierarchy order");

        TrainBuilder trainbuilder = (TrainBuilder)target;
        if (GUILayout.Button("Build Train"))
        {
            trainbuilder.BuildTrain();
        }
    }
}
