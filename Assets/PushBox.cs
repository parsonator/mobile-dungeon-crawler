﻿using UnityEngine;
using System.Collections;

public class PushBox : MonoBehaviour {

    public float Speed;
    public float SecondsDelay;

    Vector3[] Poz;

    [SerializeField]
    Transform PushPad;

    [SerializeField]
    int curruntPoint;

    Vector3 Target;

    [SerializeField]
    bool MoveActive;

    [SerializeField]
    float Length;

    

    LineRenderer line;

    // Use this for initialization
    void Start () {


        Poz = new Vector3[2];
        Poz[0] = PushPad.position;
        Poz[1] = transform.position + (transform.forward * (Length + 0.5f));



        line = GetComponent<LineRenderer>();
        line.useWorldSpace = true;



    }
	
	// Update is called once per frame
	void Update ()
    {
        if (!MoveActive)
        {
            StartCoroutine(Move(Poz[curruntPoint], Speed, SecondsDelay));
        }
        line.SetPosition(0,transform.position);
        line.SetPosition(1, PushPad.position);


    }

    public IEnumerator Move(Vector3 _MovePosition, float _Speed, float _SecondsDelay = 0)
    {
        MoveActive = true;

        Vector3 Target = _MovePosition;

        while (PushPad.position != Target)
        {
            PushPad.position = Vector3.MoveTowards(PushPad.position, Target, (_Speed * Time.deltaTime)/Length);
            yield return null;
        }
        yield return new WaitForSeconds(_SecondsDelay);
        Debug.Log("Swish");

        MoveActive = false;
        curruntPoint = curruntPoint == 0 ? 1 : 0;

        //        transform.position = ReCenterVector(transform.position + _MovePosition);
    }

    void OnDrawGizmosSelected()
    {
        if (Length != 0)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(transform.position, transform.position + (transform.forward * (Length + 0.5f)));
        }
    }
}
