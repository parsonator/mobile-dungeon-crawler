﻿using UnityEngine;
using System.Collections;

public class GlobleLayers : MonoBehaviour
{



    public static LayerMask DetectAllMask;
    public static LayerMask DetectPlayer;
    public static LayerMask DetectEneimes;

    public static LayerMask NormalMove;

    [SerializeField]
    LayerMask attackAllMask;
    [SerializeField]
    LayerMask attackPlayer;
    [SerializeField]
    LayerMask attackEneimes;
    [SerializeField]
    LayerMask normalMove;

    void Awake()
    {
        DetectAllMask = attackAllMask;
        DetectPlayer = attackPlayer;
        DetectEneimes = attackEneimes;
        DetectEneimes = attackEneimes;

        NormalMove = normalMove;

    }
}
