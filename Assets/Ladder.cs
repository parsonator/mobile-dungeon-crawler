﻿using UnityEngine;

public class Ladder : MonoBehaviour {

    public Transform LadderTop;
    public float LadderLength
    {
        get
        {
            return transform.position.y / LadderTop.position.y;
        }
    }

}
