﻿using UnityEngine;
using System.Collections;

public class HealthBox : MonoBehaviour
{

    public float Health;

    void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.layer == 10)
        {
            Debug.Log("Ding");
            other.gameObject.GetComponentInParent<Attributes>().AddHeath(Health);
            gameObject.SetActive(false);
        }
    }
}
