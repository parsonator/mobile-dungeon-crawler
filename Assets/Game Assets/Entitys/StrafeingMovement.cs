﻿using UnityEngine;
using System.Collections;

public class StrafeingMovement : MovingEntity
{
    public bool Moving = true;

    [SerializeField]
    float MoveSpeed;
    [SerializeField]
    Vector3 MoveVector;



    Attributes attributes;


    // Use this for initialization
    void Start()
    {
        attributes = GetComponent<Attributes>();
        EntityController = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!Moving) return;

        if (EntityController.isGrounded)
        {

            if (!CanMove(MoveVector, GlobleLayers.NormalMove) || CanFall(MoveVector, 0.5f, GlobleLayers.NormalMove))
            {
                MoveVector = -MoveVector;
            }
            else
            {
                EntityController.Move(MoveVector * (MoveSpeed * Time.deltaTime));
            }
        }
    }
}
