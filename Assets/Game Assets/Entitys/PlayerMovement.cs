﻿using UnityEngine;
using System.Collections;
using CnControls;

public class PlayerMovement : MovingEntity
{
    [Space(10)]
    public static PlayerMovement MainPlayer;

    public float MoveSpeed;
    public float MoveDelay;

    [SerializeField]
    bool CanRole;
    [SerializeField]
    Vector3 moveVector, LookRot, CheckPoint;

    public Attributes PlayerAttributes;


    void Awake()
    {
        EntityController = GetComponent<CharacterController>();
        CheckPoint = transform.position;
        PlayerMovement.MainPlayer = this;
        PlayerAttributes = GetComponent<Attributes>();
    }

    void Update()
    {
        if (PlayerAttributes.IsDead())
        {
            Time.timeScale = 0.2f;
            CanvasScreenManager.Master.SetScreens(CanvasScreenManager.Master.FindScreen("GameOver"));
        }


        //tempRestart
        if (transform.position.y < 0) transform.position = CheckPoint;

        Vector2 inpit = new Vector2(CnInputManager.GetAxis("Horizontal"), CnInputManager.GetAxis("Vertical"));

        moveVector.z = inpit.y;
        moveVector.x = inpit.x;

        if (moveVector != Vector3.zero)
        {

            if (!CanFall(moveVector.normalized, 0.6f, GlobleLayers.NormalMove) && EntityController.isGrounded)
                CheckPoint = transform.position;


            LookRot = moveVector.normalized;
            if (!MoveActive && EntityController.isGrounded)
            {

                //MoveTarget += moveVector;
                EntityController.Move(moveVector * (MoveSpeed * Time.deltaTime));

                if (CanFall(moveVector.normalized, 0.2f, GlobleLayers.NormalMove))
                {
                    JumpGap(moveVector, MoveSpeed, GlobleLayers.NormalMove);
                }


                if (GetLadder(moveVector, GlobleLayers.NormalMove) != null)
                {
                    Climb(GetLadder(moveVector, GlobleLayers.NormalMove));
                }

            }
        }


    }

    void Climb(Ladder _Ladder)
    {
        StartCoroutine(Climb(_Ladder.LadderTop.position, (MoveSpeed * Time.deltaTime) / _Ladder.LadderLength, MoveDelay));
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "PushPad")
        {
            StopCoroutine("Move");
            MoveActive = false;

        }
    }



    void OnCollisionStay(Collision other)
    {

        if (other.gameObject.tag == "Platform")
        {
            //transform.position = other.transform.TransformPoint(transform.position);

        }
    }

    void OnCollisionExit(Collision other)
    {
        if (other.gameObject.tag == "Platform")
        {

        }
    }

}
