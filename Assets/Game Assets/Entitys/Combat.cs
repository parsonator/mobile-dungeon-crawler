﻿using UnityEngine;
using System.Collections;

public class Combat : MonoBehaviour
{
    [Header("-Combat-")]
    public float Damage;
    public float MaxAmmo = 6;
    public float CurruntAmmo = 6;
    public float ReloadTime = 2;
    public float Range = 60;
    public bool Shooting, reloading;
    public bool CanShoot, CanExplode;

    static GameObject Shoot, Hit, Explosion;
    ParticleSystem shootFX, HitFX, ExplosionFX;
    LineRenderer shotLine;

    void Start()
    {
        GameObject Particles;
        if (Shoot == null)
        { Shoot = Resources.Load<GameObject>("Effects/ShootFX"); }

        if (Hit == null)
        { Hit = Resources.Load<GameObject>("Effects/HitFX"); }

        

        if (CanShoot)
        {
            ShootSetup();
        }



        CurruntAmmo = MaxAmmo;

    }

    public void Reload()
    {
        CurruntAmmo = MaxAmmo;
    }

    public IEnumerator SlowReload()
    {

        while (CurruntAmmo != MaxAmmo)
        {
            reloading = true;

            CurruntAmmo++;
            yield return new WaitForSeconds(ReloadTime / MaxAmmo);

        }
        reloading = false;
    }

    public Attributes ShootGun(Transform GunBarrle, float Damage, float range = 20, LayerMask Mask = new LayerMask())
    {
        CanShoot = true;
        if (shootFX == null)
            ShootSetup();

        RaycastHit hit;
        Attributes attributes;

        shootFX.transform.position = GunBarrle.position;
        shootFX.transform.rotation = Quaternion.LookRotation(GunBarrle.forward);
        shootFX.Play();

        if (Physics.Raycast(GunBarrle.position, GunBarrle.forward, out hit, range, Mask))
        {
            StartCoroutine(shootEffect(GunBarrle, hit.point));
            HitFX.transform.position = hit.point;
            HitFX.Play();

            hit.collider.gameObject.SendMessageUpwards("Damage", Damage, SendMessageOptions.DontRequireReceiver);
            if (hit.collider.gameObject.GetComponentInParent<Attributes>() != null)
            {
                attributes = hit.collider.gameObject.GetComponentInParent<Attributes>();
                CurruntAmmo--;
                return attributes; 
            }

        }
        else
        {
            StartCoroutine(shootEffect(GunBarrle, GunBarrle.position + (GunBarrle.forward * range)));
        }
        CurruntAmmo--;
        return null;
    }

    IEnumerator shootEffect(Transform Barrle, Vector3 hitPoint)
    {
        shotLine.useWorldSpace = true;
        shotLine.SetPosition(0, Barrle.position);
        shotLine.SetPosition(1, hitPoint);
        yield return new WaitForSeconds(0.05f);
        shotLine.useWorldSpace = false;
        shotLine.SetPosition(0, Barrle.position);
        shotLine.SetPosition(1, Barrle.position);
    }





    void ShootSetup()
    {
        GameObject Particles;
        Particles = Instantiate(Shoot, transform.position, Quaternion.identity) as GameObject;
        shootFX = Particles.GetComponent<ParticleSystem>();
        shootFX.transform.parent = transform;
        shotLine = shootFX.gameObject.GetComponent<LineRenderer>();

        Particles = Instantiate(Hit, transform.position, Quaternion.identity) as GameObject;
        HitFX = Particles.GetComponent<ParticleSystem>();
        HitFX.transform.parent = transform;
    }


}
