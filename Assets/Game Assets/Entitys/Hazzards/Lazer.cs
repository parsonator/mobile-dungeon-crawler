﻿using UnityEngine;
using System.Collections;
public class Lazer : MonoBehaviour
{

    public float LazerSize, lazerRange;
    public bool LazerActive;
    public GameObject hitEffect;
    public ParticleSystem hitEffectParticl;

    LineRenderer LazerLine;
    RaycastHit hit;

    // Use this for initialization
    void Start()
    {
        LazerLine = GetComponent<LineRenderer>();
        hitEffect = Instantiate(hitEffect, hit.point, transform.rotation) as GameObject;
        hitEffect.transform.parent = transform;
        hitEffectParticl = hitEffect.GetComponent<ParticleSystem>();
        LazerLine.useWorldSpace = true;

    }

    // Update is called once per frame
    void Update()
    {

        if (LazerActive)
        {
            LazerLine.enabled = true;
            Ray LazarRay = new Ray(transform.position, transform.forward);
            LazerLine.SetPosition(0, transform.position);
            if (Physics.Raycast(LazarRay, out hit, lazerRange))
            {
                if (!hitEffectParticl.isPlaying)
                    hitEffectParticl.Play();
                LazerLine.SetPosition(1, hit.point);
                hitEffect.transform.position = hit.point;
                hitEffect.transform.rotation = Quaternion.LookRotation(transform.position);
            }
            else
            {
                LazerLine.SetPosition(1, transform.position + (transform.forward * lazerRange));
                if (hitEffectParticl.isPlaying)
                    hitEffectParticl.Stop();
            }
            LazerLine.SetWidth(LazerSize, LazerSize);
        }
        else
        {
            LazerLine.enabled = false;
            if (hitEffectParticl.isPlaying)
                hitEffectParticl.Stop();
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        Gizmos.DrawLine(transform.position, transform.position + (transform.forward * lazerRange));

    }
}
