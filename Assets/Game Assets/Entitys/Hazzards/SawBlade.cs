﻿using UnityEngine;
using System.Collections;

public class SawBlade : MonoBehaviour
{

    public float Damage;

    public float Speed;
    public float SecondsDelay;
    public float Length = 7;

    [SerializeField]
    Transform[] StartAndEnd;
    [SerializeField]
    int curruntPoint;
    [SerializeField]
    Transform Blade;

    [SerializeField]
    bool MoveActive,CanKill;

    LineRenderer line;

    // Use this for initialization
    void Start()
    {
        StartAndEnd[0].position = (transform.position + (transform.right * (Length / 2)));
        StartAndEnd[1].position = (transform.position + (-transform.right * (Length / 2)));

        line = GetComponent<LineRenderer>();
        line.useWorldSpace = true;
        line.SetPosition(0, StartAndEnd[0].position);
        line.SetPosition(1, StartAndEnd[1].position);
        Blade.position = StartAndEnd[curruntPoint].position;

    }

    // Update is called once per frame
    void Update()
    {
        if (!MoveActive)
        {
            StartCoroutine(Move(StartAndEnd[curruntPoint].position, Speed, SecondsDelay));
        }

    }

    public IEnumerator Move(Vector3 _MovePosition, float _Speed, float _SecondsDelay = 0)
    {
        MoveActive = true;
        CanKill = true;
        Vector3 Target = _MovePosition;

        while (Blade.position != Target)
        {
            Blade.position = Vector3.MoveTowards(Blade.position, Target, _Speed * Time.deltaTime);
            yield return null;
        }
        CanKill = false;
        yield return new WaitForSeconds(_SecondsDelay);
        //Debug.Log("Swish");

        MoveActive = false;
        curruntPoint = curruntPoint == 0 ? 1 : 0;
    }


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 10 && CanKill)
        {
            other.gameObject.SendMessageUpwards("Damage", Damage, SendMessageOptions.DontRequireReceiver);
            Debug.Log("Slice");
        }
    }

    void OnDrawGizmosSelected()
    {
        if (StartAndEnd != null)
        {

            StartAndEnd[0].position = (transform.position + (transform.right * (Length / 2)));
            StartAndEnd[1].position = (transform.position + (-transform.right * (Length / 2)));

            Gizmos.color = Color.blue;
            Gizmos.DrawLine((transform.position + (-transform.right * (Length / 2))), (transform.position + (transform.right * (Length / 2))));
        }
    }

}
