﻿
using UnityEngine;
using System.Collections;
using System;

public class Attributes : MonoBehaviour, IKillable, IDamageable<float>
{
    public float MaxHeath;

    public float Heath;

    public int XP = 10;

    bool Dead;


    void Start()
    {
        Heath = MaxHeath;
    }

    public void Damage(float damageTaken)
    {
        Debug.Log("Hit");
        Heath -= damageTaken;

        if (gameObject.tag == "Player")
        {
            float ShakeMagnitude = Mathf.Clamp(damageTaken, 10, 50);
            StartCoroutine(PlayerCamera.Shake(0.05f, ShakeMagnitude * 0.04f));
            StartCoroutine(PlayerCamera.HitFreezeFrames((int)ShakeMagnitude / 2));
            ScoreTracker.BrakeCombo();
        }

    }

    public void Kill()
    {
        Heath = 0;
        gameObject.SetActive(false);
        Dead = true;
    }

    public bool IsDead()
    {
        return Heath <= 0;
    }

    public bool CheckDead()
    {
        if (Heath <= 0)
        {
            Kill();
            return true;
        }
        else
        {
            Dead = false;
            return false;
        }
    }

    public void Revive()
    {
        Dead = false;
        gameObject.SetActive(true);
        Heath = MaxHeath;
    }

    public float GetHeathPercent()
    {
        return Heath / MaxHeath;
    }

    public float AddHeath(float HP)
    {
        return MaxHeath = +HP + Mathf.Clamp(HP, 0, MaxHeath);
    }




}
