﻿using UnityEngine;
using System.Collections;

public class BomberCombat : Explosion
{

    public Rigidbody Bomb;

    public float DetectRange = 10;
    public float ThrowPower = 10;
    public float ThrowPersecond = 3;
    public float suicideDamage = 20;
    public Transform TrowBarrle;
    Transform Player;

    Attributes attributes;

    [SerializeField]
    bool throwing;

    // Use this for initialization
    void Start()
    {
        Player = PlayerMovement.MainPlayer.transform;
        attributes = GetComponent<Attributes>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Vector3.Distance(transform.position, Player.position) < DetectRange)
        {
            if (!throwing)
            {
                StartCoroutine(KeepThrowing());
            }
        }

        if(attributes.IsDead())
        {
            KillSelf();
        }

        if (Player.position.z > transform.position.z - 3 && !attributes.IsDead())
        {
            KillSelf();
        }


    }

    IEnumerator KeepThrowing()
    {
        if (throwing == true) yield break;
        throwing = true;
        ThrowBomb();
        yield return new WaitForSeconds(ThrowPersecond);
        throwing = false;

    }

    void ThrowBomb()
    {
        Rigidbody throwbomb = Instantiate(Bomb, TrowBarrle.position, TrowBarrle.rotation) as Rigidbody;
        throwbomb.velocity = throwbomb.transform.forward * Random.Range(ThrowPower * 0.4f, ThrowPower);

    }

    void KillSelf()
    {
        StartCoroutine(Explode(transform.position, 10, suicideDamage, GlobleLayers.DetectAllMask));
        attributes.Kill();
        gameObject.SetActive(false);

    }
}
