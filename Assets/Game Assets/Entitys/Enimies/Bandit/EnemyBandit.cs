﻿using UnityEngine;
using System.Collections;

public class EnemyBandit : Combat
{

    static public int ActiveBanditCount;

    public float ShootDelay;

    Attributes BandetStats;
    StrafeingMovement strafeingMovement;

    [SerializeField]
    Transform GunPivot;

    bool shooting;

    // Use this for initialization
    void Start()
    {
        BandetStats = GetComponent<Attributes>();
        strafeingMovement = GetComponent<StrafeingMovement>();
        ActiveBanditCount++;
    }


    // Update is called once per frame
    void Update()
    {
        if (BandetStats.IsDead())
        {
            Destroy(gameObject);
            return;
        }

        //ShootCheck
        RaycastHit hit;

        if (Physics.Raycast(GunPivot.position, GunPivot.forward, out hit, Range, GlobleLayers.DetectPlayer))
        {
                StartCoroutine(Shoot());
        }


    }

    IEnumerator Shoot()
    {
        if (shooting) yield break;

        shooting = true;
        strafeingMovement.Moving = false;
        yield return new WaitForSeconds(ShootDelay);
            ShootGun(GunPivot, Damage, Range, GlobleLayers.DetectAllMask);

        yield return new WaitForSeconds(ShootDelay);
        strafeingMovement.Moving = true;

        shooting = false;



    }

    void OnDrawGizmosSelected()
    {

        Gizmos.color = Color.red;
        Gizmos.DrawLine(GunPivot.position, GunPivot.position + (GunPivot.forward * Range));

    }
}
