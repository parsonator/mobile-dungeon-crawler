﻿using UnityEngine;
using System.Collections;

public class MovingEntity : MonoBehaviour
{
    [Header("MovingEntity Variables")]

    public bool MoveActive;
    public CharacterController EntityController;
    // public CharacterController Controller;
    public float Gravity = 1;
    public int JumpDyistence = 4;
    bool LookingActive;

    void LateUpdate()
    {
        if (!MoveActive)
        {
            EntityController.Move(Vector3.down * Gravity);
        }
    }

    public void Looking(Vector3 _Direction, float _Speed)
    {
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(_Direction), _Speed * Time.deltaTime);
    }

    public IEnumerator Move(Vector3 _MovePosition, float _Speed, float _SecondsDelay = 0)
    {
        MoveActive = true;

        Vector3 Target = ReCenterVector(_MovePosition);
        while (Vector2.Distance(XZPosition(transform.localPosition), XZPosition(Target)) >= 0.001f)
        {
            //Vector3.MoveTowards(transform.position, ReCenterVector(Target), _Speed * Time.deltaTime));

            transform.position = Vector3.MoveTowards(transform.position, new Vector3(Target.x, transform.position.y, Target.z), _Speed);

            yield return null;
        }
        if (_SecondsDelay != 0)
            yield return new WaitForSeconds(_SecondsDelay);


        MoveActive = false;
        //        transform.position = ReCenterVector(transform.position + _MovePosition);
    }

    public IEnumerator MoveTowards(Vector3 _MovePosition, float _Speed, float _SecondsDelay = 0)
    {
        MoveActive = true;

        while (transform.position != _MovePosition)
        {
            //Vector3.MoveTowards(transform.position, ReCenterVector(Target), _Speed * Time.deltaTime));

            transform.position = Vector3.MoveTowards(transform.position, _MovePosition, _Speed * Time.deltaTime);

            yield return null;
        }
        yield return new WaitForSeconds(_SecondsDelay);
        MoveActive = false;

    }

    public void JumpGap(Vector3 _MoveDirection, float _Speed, LayerMask _Mask)
    {
        Vector3 Center = transform.position;
        Center.y += 0.5f;

        RaycastHit LandHit;

        for (int i = 1; i < JumpDyistence; i++)
        {
            if (Physics.Raycast(Center + (_MoveDirection * i), Vector3.down, out LandHit, 1f, _Mask))
            {
                Debug.Log("CanJump" + LandHit.point);
                if (!MoveActive)
                    StartCoroutine(MoveTowards(LandHit.point, _Speed * 3f));
                break;
            }
        }


    }

    public IEnumerator Climb(Vector3 _MovePosition, float _Speed, float _SecondsDelay = 0)
    {
        MoveActive = true;
        //EntityController.enabled = false;

        Vector3 Target = _MovePosition;

        Vector3 startTarg = new Vector3(_MovePosition.x, transform.position.y, transform.position.z);

        while (transform.position != startTarg)
        {
            transform.position = Vector3.MoveTowards(transform.position, startTarg, _Speed);

        }

        while (transform.position.y != Target.y)
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, Target.y, transform.position.z), _Speed);
            yield return null;

        }
        while (transform.position != Target)
        {
            transform.position = Vector3.MoveTowards(transform.position, Target, _Speed);
            yield return null;

        }
        if (_SecondsDelay != 0)
            yield return new WaitForSeconds(_SecondsDelay);
        MoveActive = false;
    }

    public bool CanFall(Vector3 _MoveDirection, float _Distance, LayerMask _Mask)
    {
        Vector3 Center = transform.position;
        Center.y += 0.5f;
        Center += _MoveDirection.normalized * _Distance;

        if (Physics.Raycast(Center, Vector3.down, 1f, _Mask))
        {
            return false;
        }

        else return true;
    }

    public bool CanMove(Vector3 _MoveDirection, LayerMask _Mask)
    {
        RaycastHit hit;

        Vector3 Center = transform.position;
        Center.y += 0.5f;
        if (Physics.Raycast(Center, _MoveDirection, out hit, 1f, _Mask) && hit.collider.isTrigger == false)
        {
            return false;
        }

        else return true;

    }

    public Ladder GetLadder(Vector3 moveDi, LayerMask _Mask)
    {
        RaycastHit hit;

        Vector3 Center = transform.position;
        Center.y += 0.5f;

        if (Physics.Raycast(Center, moveDi, out hit, 0.5f, _Mask) && hit.transform.tag == "Ladder")
        {
            return hit.collider.gameObject.GetComponentInParent<Ladder>();
        }

        else return null;

    }


    public void ReCenterEntity()
    {
        transform.position = Vector3.MoveTowards(transform.position, ReCenterVector(transform.position), 2 * Time.deltaTime);
    }

    public Vector3 ReCenterVector(Vector3 Vector)
    {
        float x = (Mathf.Floor(Vector.x)) + 0.5f;
        float z = (Mathf.Floor(Vector.z)) + 0.5f;

        return new Vector3(x, Vector.y, z);
    }

    Vector2 XZPosition(Vector3 _vector3)
    {
        return new Vector3(_vector3.x, _vector3.z);
    }

}
