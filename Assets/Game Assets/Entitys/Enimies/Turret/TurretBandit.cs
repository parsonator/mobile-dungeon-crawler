﻿using UnityEngine;
using System.Collections;

public class TurretBandit : Combat
{

    [Space(10)]
    [Header("-TurretBandit-")]
  
    public float Speed;

    [SerializeField]
    Transform GunPivot;
    [SerializeField]
    Transform GunBarrle;
    [SerializeField]
    float ShotsPerSecond = 0.1f;
    [SerializeField]
    Vector3 to, from;
    [SerializeField]
    LayerMask DetectLayer;

    Attributes attributes;

    int curruntPoint;

    float t;

    void Awake()
    {
        attributes = GetComponent<Attributes>();
    }

    void Update()
    {
        attributes.CheckDead();


        if (Vector3.Distance(PlayerMovement.MainPlayer.gameObject.transform.position, transform.position) < 100)
        {
            t += Time.deltaTime;
            RaycastHit Hit;
            if (t >= ShotsPerSecond)
            {
                ShootGun(GunBarrle, Damage, Range,DetectLayer);
                t = 0;
            }
            OssulateRotation();
        }



    }

    void OssulateRotation()
    {

            float t = Mathf.PingPong(Time.time * Speed * 2.0f, 1.0f);
            GunPivot.localEulerAngles = Vector3.Lerp(from, to, t);


    }
}
