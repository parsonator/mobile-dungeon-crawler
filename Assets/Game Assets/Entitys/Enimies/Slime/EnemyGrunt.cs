﻿
using UnityEngine;
using System.Collections;
using System;

public class EnemyGrunt : MovingEntity
{
    static public int ActiveGruntCount;
    public float MoveSpeed, Damage, ChaseDistence, StrafeAmount;
    [SerializeField]
    LayerMask Mask;
    [SerializeField]
    Vector3 moveVector, LookRot;

    Attributes GruntHeath;
    float Distance;
    Transform Player;

    // Use this for initialization
    void Start()
    {
        GruntHeath = GetComponent<Attributes>();
        EntityController = GetComponent<CharacterController>();
        Player = PlayerMovement.MainPlayer.transform;
        ActiveGruntCount++;
    }

    // Update is called once per frame
    void Update()
    {
        if (GruntHeath.IsDead())
        {
            Destroy(gameObject);
            return;
        }


        if (!MoveActive && EntityController.isGrounded)
        {

            if (Vector3.Distance(transform.position, Player.position) < ChaseDistence)
            {
                moveVector = (Player.position - transform.position).normalized;
                moveVector += (Vector3.right * (Mathf.PingPong(Time.time * StrafeAmount * 0.5f, 1) - (StrafeAmount / 2)));
                Looking(LookRot, 10);

                if (CanFall(moveVector.normalized, 0.4f, Mask))
                {
                    JumpGap(moveVector.normalized, MoveSpeed, Mask);
                }

                LookRot = moveVector;
                if (!CanFall(moveVector.normalized, 0.2f, Mask))
                    EntityController.Move(moveVector * (MoveSpeed * Time.deltaTime));
            }
        }

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 10)
        {
            other.gameObject.SendMessageUpwards("Damage", Damage, SendMessageOptions.DontRequireReceiver);
            Debug.Log("Slice");
            Destroy(gameObject);

        }
    }
}

