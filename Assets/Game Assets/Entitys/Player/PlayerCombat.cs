﻿using UnityEngine;
using System.Collections;
using CnControls;

public class PlayerCombat : Combat
{
    [Space(10)]
    [Header("-PlayerCombat-")]

    public float ShotDelay;
    public static PlayerCombat MainPlayerCombat;

    public Transform GunBarrle;

    void Awake()
    {
        MainPlayerCombat = this;
    }
    // Update is called once per frame
    void Update()
    {
        if (CnInputManager.GetButtonDown("Jump"))
        {
            if (reloading) return;
            StartCoroutine(QuickDrawShoot(GunBarrle, Damage, Range, ShotDelay, GlobleLayers.DetectAllMask));
        }
        if (CurruntAmmo <= 0)
        {
            StartCoroutine(SlowReload());
        }
    }

    public IEnumerator QuickDrawShoot(Transform GunBarrle, float Damage, float range, float ShootDelay = 0, LayerMask Mask = new LayerMask())
    {
        if (Shooting) yield break;

        Shooting = true;
        Attributes EnemyHit = ShootGun(GunBarrle, Damage, range, Mask);

        if (EnemyHit != null)
        {
            ScoreTracker.AddHitScore(EnemyHit.XP);
            float ShakeMagnitude = Mathf.Clamp(ScoreTracker.HitCombo, 1, 50);
            StartCoroutine(PlayerCamera.Shake(0.03f,ShakeMagnitude * 0.05f));
            StartCoroutine(PlayerCamera.HitFreezeFrames(5));
        }
        else
        {
            ScoreTracker.BrakeCombo();
        }

        yield return new WaitForSeconds(ShootDelay);
        Shooting = false;
    }

    /*
    IEnumerator Reload()
    {
        while (CurruntAmmo != MaxAmmo)
        {
            reloading = true;

            CurruntAmmo++;
            yield return new WaitForSeconds(ReloadTime / MaxAmmo);
        }
        reloading = false;
    }
    */
}
