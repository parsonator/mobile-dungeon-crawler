﻿using UnityEngine;
using System.Collections;

public class PlayerCamera : MonoBehaviour
{

    public Vector3 CamCenter;

    public static PlayerCamera MainCam;

    public Vector3 CamOffset, LookOffset;
    public float Smooth, Xmultiplyer, MinLOS, MaxLOS;
    public LayerMask LookMask;
    public Transform player;

    public bool trackPlayer, LookAtPlayer;


    //Camera PlayerCam;

    // Use this for initialization
    void Start()
    {
        MainCam = this;
        if (player == null)
            player = PlayerMovement.MainPlayer.gameObject.transform;
        transform.position = player.position - CamOffset;
        // PlayerCam = Camera.main;
    }

    void LateUpdate()
    {
        if (trackPlayer)
        {

            if (Physics.Linecast(transform.position, player.position + (Vector3.up * 0.5f), LookMask))
            {
                CamOffset.z -= 7 * Time.deltaTime;
            }
            else if (!Physics.Linecast(transform.position + (-Vector3.forward), player.position + (Vector3.up * 0.5f), LookMask))
            {
                CamOffset.z += 7 * Time.deltaTime;
            }
            CamOffset.z = Mathf.Clamp(CamOffset.z, MinLOS, MaxLOS);

            CamOffset.x = (CamCenter.x - -player.position.x) * Xmultiplyer;
            transform.position = Vector3.Slerp(transform.position, (player.position - CamOffset), Smooth * Time.deltaTime);
        }
        if (LookAtPlayer)
        {
            Vector3 LookPoz = player.position;
            transform.LookAt(LookPoz + LookOffset);
        }
    }

    public static IEnumerator HitFreezeFrames(float Frames = 3)
    {
        Time.timeScale = 0;
        for (int i = 0; i < Frames; i++)
        {
            yield return new WaitForEndOfFrame();
        }
        Time.timeScale = 1;

    }

    public static IEnumerator Shake(float duration = 0.05f, float magnitude = 0.5f)
    {
        // trackPlayer = false;
        float elapsed = 0.0f;


        while (elapsed < duration)
        {
            Vector3 originalCamPos = MainCam.transform.position;

            elapsed += Time.deltaTime;

            float percentComplete = elapsed / duration;
            float damper = 1.0f - Mathf.Clamp(4.0f * percentComplete - 3.0f, 0.0f, 1.0f);

            // map value to [-1, 1]
            float x = Random.value * 2.0f - 1.0f;
            float y = Random.value * 2.0f - 1.0f;
            x *= magnitude * damper;
            y *= magnitude * damper;

            MainCam.transform.position = new Vector3(originalCamPos.x + x, originalCamPos.y + y, originalCamPos.z);

            yield return null;
        }
        //trackPlayer = true;
    }

}
