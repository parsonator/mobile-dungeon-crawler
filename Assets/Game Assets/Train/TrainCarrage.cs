﻿using UnityEngine;
using System.Collections;

public class TrainCarrage : MonoBehaviour
{
    public float CarrageLenght
    {
        get
        {
            if (MainTrainMesh != null)
                return MainTrainMesh.bounds.size.z;
            else return 0;
        }
    }

    public Vector3 MeshCenter
    {
        get
        { return transform.InverseTransformPoint(MainTrainMesh.bounds.center); }
    }
    public MeshRenderer MainTrainMesh;
    public Dificalty CarrageDificalty;

    void OnDrawGizmosSelected()
    {
        switch (CarrageDificalty)
        {
            case Dificalty.Easy:
                Gizmos.color = Color.green;
                break;
            case Dificalty.Medium:
                Gizmos.color = Color.yellow;

                break;
            case Dificalty.Hard:
                Gizmos.color = Color.red;

                break;
            case Dificalty.Expert:
                Gizmos.color = Color.white;

                break;
            case Dificalty.Boss:
                Gizmos.color = Color.cyan;

                break;
            default:
                break;
        }
        Gizmos.DrawWireCube(transform.position + ((Vector3.forward * CarrageLenght) / 2), new Vector3(10, 0, CarrageLenght));
    }

    public enum Dificalty
    {
        Easy,
        Medium,
        Hard,
        Expert,
        Boss
    }
}
