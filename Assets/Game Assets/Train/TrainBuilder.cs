﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
[ExecuteInEditMode]
public class TrainBuilder : MonoBehaviour {

    public List<TrainCarrage> TrainOrder;

    public Vector3 StartLocation;

    public float TrainSpaceing;

    public float TotalTrainLenght; 

    public void BuildTrain()
    {
        Debug.Log("Build Train");

        Component[] Carrage = GetComponentsInChildren<TrainCarrage>();
        List<TrainCarrage> car = new List<TrainCarrage>();
        foreach (Component t in Carrage)
        {
            car.Add(t.GetComponent<TrainCarrage>());
        }
        TrainOrder = car;

        Vector3 lastCarragePoz = StartLocation;
        for (int i = 0; i < TrainOrder.Count; i++)
        {       
            Vector3 trainPoz = lastCarragePoz + (Vector3.forward * TrainSpaceing);
            TrainOrder[i].MainTrainMesh.transform.localPosition = new Vector3(0,-2 , TrainOrder[i].CarrageLenght/2) ;
            TrainOrder[i].gameObject.transform.position = trainPoz;
            TotalTrainLenght += TrainOrder[i].CarrageLenght;
            TotalTrainLenght += TrainSpaceing;
            lastCarragePoz = trainPoz + (Vector3.forward * TrainOrder[i].CarrageLenght);
        }
    }
}


